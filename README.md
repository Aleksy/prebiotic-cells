# Prebiotic Cells

Life simulator on [Prebiotic Framework](https://gitlab.com/Aleksy/prebiotic-framework) engine.
That's the test simulator where I'll try to implement the synthetic life (cells).

# Installation

In a nearest future I'll prepare the download file.

Actual version: 0.0.1

# More

The simulator is written parallel with a next posts on the blog
[Spektrum Abstrakcji](https://spektrumabstrakcji.wordpress.com/)
(Polish language).