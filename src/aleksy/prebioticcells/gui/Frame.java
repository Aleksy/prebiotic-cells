package aleksy.prebioticcells.gui;

import aleksy.prebioticcells.constants.BasicConstants;
import aleksy.prebioticframework.vision.common.constants.VisionConstants;

import javax.swing.*;

public class Frame extends JFrame {
   private ContentPane contentPane;

   public Frame(ContentPane contentPane) {
      super(BasicConstants.NAME + " " + BasicConstants.VERSION);
      this.contentPane = contentPane;
      init();
      setContentPane(contentPane);
      setUndecorated(true);
      setExtendedState(MAXIMIZED_BOTH);
      setVisible(true);
      setFocusable(true);
      // todo save before exit
      setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      setLayout(null);
   }

   private void init() {
      contentPane.setLayout(null);
      contentPane.setSize(getSize());
      contentPane.setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
   }
}
