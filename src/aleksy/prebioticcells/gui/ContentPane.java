package aleksy.prebioticcells.gui;

import aleksy.prebioticcells.model.general.Something;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ContentPane extends JPanel {
   private List<Something> somethings;

   public ContentPane(List<Something> somethings) {
      this.somethings = somethings;
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      for (Something something : somethings) {
         something.draw(g);
      }
   }
}
