package aleksy.prebioticcells.di;

import aleksy.prebioticcells.logic.Processor;
import aleksy.prebioticframework.common.api.PrebioticApi;
import aleksy.prebioticframework.common.impl.Prebiotic;

import java.util.Random;

public class Di {
   public static final PrebioticApi prebiotic = Prebiotic.newInstance();
   public static final Processor processor = new Processor();
   public static final Random random = new Random();

   public static void init() {
      prebiotic.enableEventLogging();
   }
}
