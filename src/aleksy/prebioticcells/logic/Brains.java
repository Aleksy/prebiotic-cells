package aleksy.prebioticcells.logic;

import aleksy.prebioticcells.di.Di;
import aleksy.prebioticframework.evolution.genetic.common.model.brain.Brain;
import aleksy.prebioticframework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

public class Brains {
   public static Brain createThinkerBrain() throws EmptyFunctionDefinitionException, InvalidStructureException {
      Brain brain = new Brain();
      NeuralNetwork neuralNetwork = Di.prebiotic.neuralApi().getNeuralNetworkBuilder(
         Di.prebiotic.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setAxonFunction(AxonFunctionRecipe.TANH)
            .create()
      ).addDefaultInputLayer(24, 37)
         .addLayer(2)
         .endBuildingNetwork()
         .randomizeWeights(-0.5, 0.5)
         .create();
      brain.getNeuralNetworks().add(neuralNetwork);
      return brain;
   }
}
