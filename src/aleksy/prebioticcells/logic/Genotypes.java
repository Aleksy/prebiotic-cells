package aleksy.prebioticcells.logic;

import aleksy.prebioticcells.di.Di;
import aleksy.prebioticframework.common.model.Range;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;

public class Genotypes {
   public static Genotype createThinkerGenotype() {
      return Di.prebiotic.evolutionApi().getGenotypeBuilder()
         .addChromosome("look")
            .addGene(0.53 + 0.055 * Di.random.nextDouble(), new Range(0.53, 0.57), "color_hue")
            .addGene(0.6 + 0.1 * Di.random.nextDouble(), new Range(0.6, 0.7), "color_saturation")
            .addGene(0.45 + 0.12 * Di.random.nextDouble(), new Range(0.45, 0.57), "color_brightness").end()
         .addChromosome("features")
            .addGene(6 + 2 * Di.random.nextDouble(), new Range(6.0, 8.0), "max_speed")
            .addGene(20 + 10 * Di.random.nextDouble(), new Range(20, 30), "max_age")
            .addGene(0.02 + 0.01 * Di.random.nextDouble(), new Range(0.02, 0.03), "hunger_process_rate")
            .addGene(10.0 + 4 * Di.random.nextDouble(), new Range(10.0, 14.0), "max_stamina")
            .addGene(0.01, new Range(0.005, 0.2), "turn_speed")
            .addGene(0.01, new Range(0.005, 0.2), "fauna_digestion_quality")
            .addGene(0.99, new Range(0.9, 1.0), "flora_digestion_quality").end()
         .addChromosome("physics")
            .addGene(3.0 + 8 * Di.random.nextDouble(), new Range(3.0, 14.0), "nutritional_value").end()
         .create();
   }

}
