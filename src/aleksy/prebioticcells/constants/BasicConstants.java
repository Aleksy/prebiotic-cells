package aleksy.prebioticcells.constants;

public class BasicConstants {
   /**
    * Name
    */
   public static final String NAME = "Prebiotic Cells";
   /**
    * Author
    */
   public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
   /**
    * Version
    */
   public static final String VERSION = "0.0.1";
}
