package aleksy.prebioticcells.constants;

/**
 * Basic constants class
 */
public class TechnicalConstants {
   /**
    * The time quantum of simulation
    */
   public static final double QUANTUM = 0.1;

   /**
    * Process thread sleep time
    */
   public static final int PROCESS_THREAD_SLEEP_TIME = 7;

   /**
    * Repainter thread sleep time
    */
   public static final int REPAINTER_THREAD_SLEEP_TIME = 7;

   /**
    * Aging rate
    */
   public static final double AGING_RATE = 0.001;

   /**
    * Hunger rate
    */
   public static final double HUNGER_RATE = 0.001;

   /**
    * Stamina rate
    */
   public static final double STAMINA_RATE = 0.001;

   /**
    * Pheromone numeric code length
    */
   public static final int PHEROMONE_LENGTH = 32;
}
