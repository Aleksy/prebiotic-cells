package aleksy.prebioticcells;

import aleksy.prebioticcells.constants.TechnicalConstants;
import aleksy.prebioticcells.gui.ContentPane;
import aleksy.prebioticcells.gui.Frame;
import aleksy.prebioticcells.logic.Brains;
import aleksy.prebioticcells.logic.Genotypes;
import aleksy.prebioticcells.model.fauna.Thinker;
import aleksy.prebioticcells.model.flora.Flon;
import aleksy.prebioticcells.model.flora.Plank;
import aleksy.prebioticcells.model.flora.Rotun;
import aleksy.prebioticcells.model.general.Living;
import aleksy.prebioticcells.model.general.Something;
import aleksy.prebioticframework.common.util.EuclideanDistance;
import aleksy.prebioticframework.common.util.ListCreator;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.InvalidStructureException;

import java.util.ArrayList;
import java.util.List;

public class CellsApp {

   public static void main(String[] args) throws EmptyFunctionDefinitionException, InvalidStructureException {
      List<Something> somethings = new ArrayList<>();
      for (int i = 0; i < 30; i++) {
         somethings.add(new Plank()
            .randomizePosition()
            .randomMove());
         somethings.add(new Flon()
            .randomizePosition()
            .randomMove());
         somethings.add(new Rotun()
            .randomizePosition()
            .randomMove());
         somethings.add(new Thinker(Genotypes.createThinkerGenotype(), Brains.createThinkerBrain())
            .randomizePosition());

      }
      prepareProcessorThread(somethings).start();
      ContentPane contentPane = new ContentPane(somethings);
      prepareRepainterThread(contentPane).start();
      new Frame(contentPane);
   }

   private static Thread prepareRepainterThread(ContentPane contentPane) {
      return new Thread(() -> {
         while (!Thread.interrupted()) {
            contentPane.repaint();
            try {
               Thread.sleep(TechnicalConstants.REPAINTER_THREAD_SLEEP_TIME);
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
         }
      });
   }

   private static Thread prepareProcessorThread(List<Something> somethings) {
      return new Thread(() -> {
         while (!Thread.interrupted()) {
            somethings.forEach(something -> something.process(
               createListOfOtherSomethings(something, somethings),
               calculateDistances(something, somethings)));
            List<Integer> indexesToRemove = new ArrayList<>();
            somethings.forEach(something -> {
               if (something.isLiveryMatter()) {
                  if (!((Living) something).lives()) {
                     indexesToRemove.add(somethings.indexOf(something));
                  }
               }
            });
            indexesToRemove.forEach(index -> somethings.remove(index.intValue()));
            try {
               Thread.sleep(TechnicalConstants.PROCESS_THREAD_SLEEP_TIME);
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
         }
      });
   }

   private static List<Double> calculateDistances(Something something, List<Something> somethings) {
      List<Double> distances = new ArrayList<>();
      somethings.forEach(otherSomething -> distances.add(EuclideanDistance.between(
         ListCreator.createFrom(something.getX(), something.getY()),
         ListCreator.createFrom(otherSomething.getX(), otherSomething.getY())
      )));
      return distances;
   }

   private static List<Something> createListOfOtherSomethings(Something basicSomething, List<Something> somethings) {
      List<Something> otherSomethings = new ArrayList<>();
      somethings.forEach(something -> {
         if (!something.equals(basicSomething)) {
            otherSomethings.add(something);
         }
      });
      return otherSomethings;
   }
}
