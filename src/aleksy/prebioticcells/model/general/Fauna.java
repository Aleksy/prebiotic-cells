package aleksy.prebioticcells.model.general;

import aleksy.prebioticcells.constants.TechnicalConstants;
import aleksy.prebioticcells.enumerate.SomethingType;
import aleksy.prebioticcells.model.living.Pheromone;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Protista;

import java.util.List;

public abstract class Fauna extends Living {
   protected int maxAge;
   private double age;
   protected double maxSpeed;
   protected double faunaDigestionQuality;
   protected double floraDigestionQuality;
   protected double maxStamina;
   protected double stamina;
   protected double hunger;
   protected double hungerProcessRate;
   protected Protista protista;
   protected double turn;
   protected double turnSpeed;

   public Fauna(String pheromoneCode) {
      super(new Pheromone(pheromoneCode));
      somethingType = SomethingType.FAUNA;
      dead = false;
   }

   public void init() {
      age = 0.0;
      stamina = maxStamina;
      hunger = 0.0;
      speed = 0;
      direction = 0.0;
      turn = 0.0;
   }

   @Override
   public synchronized void process(List<Something> somethings, List<Double> distances) {
      if (!dead) {
         super.process(somethings, distances);
         age += TechnicalConstants.AGING_RATE;
         hunger += TechnicalConstants.HUNGER_RATE * hungerProcessRate;
         if (hunger > 1.0) hunger = 1.0;
         stamina -= TechnicalConstants.STAMINA_RATE * hunger;
         if (stamina > maxStamina) stamina = maxStamina;
         if (hunger < 0.0) hunger = 0.0;
         if (deathCondition())
            kill();
      }
   }

   private boolean deathCondition() {
      return stamina < 0.0 || age > maxAge;
   }
}
