package aleksy.prebioticcells.model.general;

import aleksy.prebioticcells.enumerate.SomethingType;
import aleksy.prebioticcells.model.living.Pheromone;

import java.util.List;

public abstract class Flora extends Living {
   public Flora(String pheromoneCode) {
      super(new Pheromone(pheromoneCode));
      somethingType = SomethingType.FLORA;
   }

   @Override
   public synchronized void process(List<Something> somethings, List<Double> distances) {
      if(nutritionalValue <= 0.0) {
         kill();
      }
      super.process(somethings, distances);
   }

   public double subtractNutritionalValue(double valueToSubtract) {
      if (nutritionalValue > valueToSubtract) {
         nutritionalValue -= valueToSubtract;
         return valueToSubtract;
      }
      if (nutritionalValue <= valueToSubtract) {
         valueToSubtract = nutritionalValue;
         nutritionalValue = 0.0;
         return valueToSubtract;
      }
      return 0.0;
   }
}
