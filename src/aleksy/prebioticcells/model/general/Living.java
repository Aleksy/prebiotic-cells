package aleksy.prebioticcells.model.general;

import aleksy.prebioticcells.model.living.Pheromone;

import java.awt.*;
import java.util.Random;

public abstract class Living extends Something {
   private Pheromone pheromone;
   protected double nutritionalValue;
   protected Color mainColor;
   protected boolean dead;

   Living(Pheromone pheromone) {
      this.pheromone = pheromone;
      liveryMatter = true;
   }

   protected void assignBasicParameters(float hue, float saturation, float brightness, double nutritionalValue) {
      mainColor = new Color(Color.HSBtoRGB(hue, saturation, brightness));
      this.nutritionalValue = nutritionalValue;
   }

   public Pheromone getPheromone() {
      return pheromone;
   }

   public boolean lives() {
      return !dead;
   }

   void kill() {
      dead = true;
   }
}
