package aleksy.prebioticcells.model.general;

import aleksy.prebioticcells.constants.TechnicalConstants;
import aleksy.prebioticcells.enumerate.SomethingType;

import java.awt.*;
import java.util.List;

public abstract class Something {
   protected double x;
   protected double y;
   protected double speed;
   protected double direction;
   boolean liveryMatter;
   SomethingType somethingType;

   public synchronized void process(List<Something> somethings, List<Double> distances) {
      if (speed > 0) {
         x += speed * Math.sin(direction * 2 * Math.PI) * TechnicalConstants.QUANTUM;
         y += speed * Math.cos(direction * 2 * Math.PI) * TechnicalConstants.QUANTUM;
      }
   }

   public double getX() {
      return x;
   }

   public double getY() {
      return y;
   }

   public SomethingType getSomethingType() {
      return somethingType;
   }

   public boolean isLiveryMatter() {
      return liveryMatter;
   }

   public abstract void draw(Graphics g);
}
