package aleksy.prebioticcells.model.living;

import java.util.ArrayList;
import java.util.List;

public class Pheromone {
   private String code;

   public Pheromone(String code) {
      this.code = code;
   }

   public String getCode() {
      return code;
   }

   public List<Double> generateNumberCode() {
      List<Double> doubles = new ArrayList<>();
      for (Byte b : code.getBytes()) {
         doubles.addAll(convertLetterToListOfDouble((byte) Math.abs(b)));
      }
      return doubles;
   }

   private List<Double> convertLetterToListOfDouble(byte letter) {
      String binaryLetter = Integer.toBinaryString(letter);
      if (binaryLetter.length() < 8) {
         StringBuilder before = new StringBuilder();
         for (int i = binaryLetter.length(); i < 8; i++) {
            before.append("0");
         }
         binaryLetter = before.toString() + binaryLetter;
      }
      if (binaryLetter.length() > 8) {
         System.out.println();
      }
      List<Double> doubles = new ArrayList<>();
      for (Character c : binaryLetter.toCharArray()) {
         if (c.equals('0'))
            doubles.add(-1.0);
         if (c.equals('1'))
            doubles.add(1.0);
      }
      return doubles;
   }
}
