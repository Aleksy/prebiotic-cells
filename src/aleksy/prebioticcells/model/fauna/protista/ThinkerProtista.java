package aleksy.prebioticcells.model.fauna.protista;

import aleksy.prebioticcells.constants.TechnicalConstants;
import aleksy.prebioticcells.model.general.Living;
import aleksy.prebioticcells.model.general.Something;
import aleksy.prebioticframework.evolution.genetic.common.model.brain.Brain;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Protista;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThinkerProtista extends Protista {
   public ThinkerProtista(Genotype genotype) {
      super(genotype);
   }

   public ThinkerProtista(Genotype genotype, Brain brain) {
      super(genotype, brain);
   }

   public List<Double> think(Something nearestSomething,
                             double bestDistance,
                             double hunger,
                             double stamina,
                             double speed,
                             double turn) throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException {
      List<Double> inputs = createInputs(nearestSomething, bestDistance, hunger, stamina, speed, turn);
      return brain.getNeuralNetworks().get(0).f(inputs);
   }

   private List<Double> createInputs(Something nearestSomething, double distance, double hunger, double stamina, double speed, double turn) {
      List<Double> inputs = new ArrayList<>();
      if(nearestSomething.isLiveryMatter()) {
         inputs.addAll(((Living)nearestSomething).getPheromone().generateNumberCode());
      } else {
         for(int i = 0; i < TechnicalConstants.PHEROMONE_LENGTH; i++)
            inputs.add(0.0);
      }
      inputs.addAll(Arrays.asList(distance, hunger, stamina, speed, turn));
      return inputs;
   }

   @Override
   public Geneticable newInstance(Genotype genotype) {
      return new ThinkerProtista(genotype);
   }
}
