package aleksy.prebioticcells.model.fauna;

import aleksy.prebioticcells.enumerate.SomethingType;
import aleksy.prebioticcells.model.fauna.protista.ThinkerProtista;
import aleksy.prebioticcells.model.general.Fauna;
import aleksy.prebioticcells.model.general.Flora;
import aleksy.prebioticcells.model.general.Something;
import aleksy.prebioticframework.common.util.VectorNormalizator;
import aleksy.prebioticframework.evolution.genetic.common.model.brain.Brain;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;

import java.awt.*;
import java.util.List;
import java.util.Random;

public class Thinker extends Fauna {

   public Thinker(Genotype genotype, Brain brain) {
      super("ntxl");
      assignBasicParameters(
         genotype.findGeneValue("look:color_hue").floatValue(),
         genotype.findGeneValue("look:color_saturation").floatValue(),
         genotype.findGeneValue("look:color_brightness").floatValue(),
         genotype.findGeneValue("physics:nutritional_value"));
      maxSpeed = genotype.findGeneValue("features:max_speed");
      maxAge = genotype.findGeneValue("features:max_age").intValue();
      hungerProcessRate = genotype.findGeneValue("features:hunger_process_rate");
      maxStamina = genotype.findGeneValue("features:max_stamina");
      turnSpeed = genotype.findGeneValue("features:turn_speed");
      faunaDigestionQuality = genotype.findGeneValue("features:fauna_digestion_quality");
      floraDigestionQuality = genotype.findGeneValue("features:flora_digestion_quality");
      protista = new ThinkerProtista(genotype, brain);
      init();
   }

   public Thinker randomizePosition() {
      Random random = new Random();
      x = 1800 * random.nextDouble();
      y = 1000 * random.nextDouble();
      return this;
   }

   @Override
   public synchronized void process(List<Something> somethings, List<Double> distances) {
      try {
         double bestDistance = Double.MAX_VALUE;
         Something nearestSomething = null;
         for (int i = 0; i < somethings.size(); i++) {
            if (distances.get(i) < bestDistance) {
               bestDistance = distances.get(i);
               nearestSomething = somethings.get(i);
            }
         }
         List<Double> thinks = ((ThinkerProtista) protista).think(nearestSomething, bestDistance, hunger, stamina, speed, turn);
         speed = maxSpeed * VectorNormalizator.setValueBetweenZeroAndOne(thinks.get(0), -1.0, 1.0);
         turn = thinks.get(1);
         direction += turn * turnSpeed;
         if (nearestSomething != null)
            if (nearestSomething.getSomethingType() == SomethingType.FLORA) {
               if (hunger > 0) {
                  double eat = ((Flora) nearestSomething).subtractNutritionalValue(hunger);
//                  hunger -= eat;
               }
            }
      } catch (IncorrectNumberOfInputsInLayerException | IncorrectNumberOfInputsInNeuronException e) {
         e.printStackTrace();
      }
      super.process(somethings, distances);
   }

   @Override
   public void draw(Graphics g) {
      int size = 14;
      g.setColor(mainColor);
      g.fillOval((int) x - size / 2, (int) y - size / 2, size, size);
      g.setColor(mainColor.brighter().brighter());
      g.drawOval((int) x - size / 2, (int) y - size / 2, size, size);
   }
}
