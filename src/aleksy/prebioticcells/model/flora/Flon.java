package aleksy.prebioticcells.model.flora;

import aleksy.prebioticcells.di.Di;
import aleksy.prebioticcells.model.general.Flora;

import java.awt.*;
import java.util.Random;

public class Flon extends Flora {
   public Flon() {
      super("flfl");
      assignBasicParameters(
         0.36f + 0.055f * Di.random.nextFloat(),
         0.55f + 0.1f * Di.random.nextFloat(),
         0.44f + 0.14f * Di.random.nextFloat(),
         2.0 + 6 * Di.random.nextDouble());
   }

   public Flon randomizePosition() {
      Random random = new Random();
      x = 1800 * random.nextDouble();
      y = 1000 * random.nextDouble();
      return this;
   }

   public Flon randomMove() {
      Random random = new Random();
      direction = random.nextDouble();
      speed = 0.1 * random.nextDouble();
      return this;
   }

   @Override
   public void draw(Graphics g) {
      if (lives()) {
         double size = 6 + nutritionalValue * 0.7;
         g.setColor(mainColor);
         g.fillOval((int) (x - (size)), (int) (y - (size * 0.5)), (int) (size * 2), (int) (size));
         g.fillOval((int) (x - (size * 0.5)), (int) (y - (size)), (int) (size), (int) (size * 2));
      }
   }
}
