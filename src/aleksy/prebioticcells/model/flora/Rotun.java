package aleksy.prebioticcells.model.flora;

import aleksy.prebioticcells.di.Di;
import aleksy.prebioticcells.model.general.Flora;

import java.awt.*;
import java.util.Random;

public class Rotun extends Flora {
   public Rotun() {
      super("flfl");
      assignBasicParameters(
         0.26f + 0.015f * Di.random.nextFloat(),
         0.75f + 0.13f * Di.random.nextFloat(),
         0.14f + 0.14f * Di.random.nextFloat(),
         1.0 + 2.5 * Di.random.nextDouble());
   }

   public Rotun randomizePosition() {
      Random random = new Random();
      x = 1800 * random.nextDouble();
      y = 1000 * random.nextDouble();
      return this;
   }

   public Rotun randomMove() {
      Random random = new Random();
      direction = random.nextDouble();
      speed = 0.1 * random.nextDouble();
      return this;
   }

   @Override
   public void draw(Graphics g) {
      if (lives()) {
         double size = 8 + nutritionalValue * 0.7;
         g.setColor(mainColor);
         g.fillOval((int) (x - (size * 0.5)), (int) (y - (size * 0.5)), (int) (size), (int) (size));
         g.setColor(mainColor.brighter());
         g.drawOval((int) (x - (size * 0.5)), (int) (y - (size * 0.5)), (int) (size), (int) (size));
      }
   }
}
