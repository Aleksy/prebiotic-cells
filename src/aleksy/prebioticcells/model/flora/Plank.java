package aleksy.prebioticcells.model.flora;

import aleksy.prebioticcells.di.Di;
import aleksy.prebioticcells.model.general.Flora;

import java.awt.*;
import java.util.Random;

public class Plank extends Flora {
   public Plank() {
      super("flpl");
      assignBasicParameters(
         0.33f + 0.055f * Di.random.nextFloat(),
         0.7f + 0.1f * Di.random.nextFloat(),
         0.4f + 0.2f * Di.random.nextFloat(),
         3.0 + 8 * Di.random.nextDouble());
   }

   public Plank randomizePosition() {
      Random random = new Random();
      x = 1800 * random.nextDouble();
      y = 1000 * random.nextDouble();
      return this;
   }

   public Plank randomMove() {
      Random random = new Random();
      direction = random.nextDouble();
      speed = 0.1 * random.nextDouble();
      return this;
   }

   @Override
   public void draw(Graphics g) {
      if (lives()) {
         double size = 5 + nutritionalValue * 0.8;
         g.setColor(mainColor);
         g.fillOval((int) (x - (size * 0.4)), (int) (y - (size * 0.4)), (int) (size), (int) (size));
         g.fillOval((int) (x - (size * 0.4)), (int) (y + (size * 0.4)), (int) (size), (int) (size));
         g.fillOval((int) (x + (size * 0.4)), (int) (y - (size * 0.4)), (int) (size), (int) (size));
         g.fillOval((int) (x + (size * 0.4)), (int) (y + (size * 0.4)), (int) (size), (int) (size));
      }
   }
}
